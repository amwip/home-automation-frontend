#include <ESP8266HTTPClient.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiServerSecure.h>
#include <WiFiUdp.h>


#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <DHT.h>
// Set these to run example.
#define FIREBASE_HOST "smart-home-f6788.firebaseio.com" //link do projektu w firebase
#define FIREBASE_AUTH "eVO0O1BWTwMDQxgwLZyobzb3aRvW0jEjOv1n980x"   //firebase auth token do bazy
#define WIFI_SSID  "OnePlus 6" //Nazwa sieci
#define WIFI_PASSWORD "8146da9ead7b" //hasło do sieci
#define DHTTYPE DHT11   // DHT 11
#define DHTPIN 14 //pin do którego jest podpiete dht

DHT dht(DHTPIN, DHTTYPE); //init value

 
  void setup() {
  Serial.begin(115200);  
  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
  Serial.print(".");
  delay(500);
  }
  delay(1000);
  Serial.println();
Serial.println(WiFi.localIP());

Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop() {
Serial.println("start");
if (Firebase.failed()) {
Serial.print("setting /message failed:");
Serial.println(Firebase.error());
return;
}

HTTPClient http;
http.begin("http://am138615.000webhostapp.com/time.php");
int httpCode = http.GET();
String payload = http.getString();
http.end();

String timestamp = payload.substring(9,19);
Serial.println(timestamp);

 //read DHT sensor very 2sec
  float h = 0.00f;
  float t = 0.00f;
  h = dht.readHumidity();
  t = dht.readTemperature();
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& valueObject = jsonBuffer.createObject();
  valueObject["temperature"] = (String) t;
  valueObject["humidity"] = (String) h;
  valueObject["macAddress"] = (String) WiFi.macAddress();
  valueObject["timestamp"] = timestamp;
  Serial.println(t);
  Serial.println(h);
  
  Firebase.set("devices/", valueObject["macAddress"]);

Firebase.set("dht11/value/" + timestamp, valueObject);
  delay(2000);
}
