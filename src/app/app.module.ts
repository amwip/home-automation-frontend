// MODULES
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { MaterializeModule } from 'angular2-materialize';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// COMPONENTS
import { AppComponent } from './app.component';
import { LoginComponent } from '../components/login/login.component';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { WelcomeComponent } from '../components/welcome/welcome.component';
import { PanelComponent } from '../components/panel/panel.component';
import { ChartViewComponent } from '../components/chart-view/chart-view.component';

// SERVICES / PROVIDERS
import { AuthorizationService } from '../providers/authorization.service';
import { AuthGuard } from '../providers/authGuard.service';
import { ChartService } from '../providers/chart.service';

// OTHER
import { FIREBASE_CONFIG } from '../environments/environment';



const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'panel', component: PanelComponent, canActivate: [AuthGuard]  },
  { path: '', component: WelcomeComponent },


  // { path: 'hero/:id',      component: HeroDetailComponent },
  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // },
  // { path: '',
  //   redirectTo: '/login',
  //   pathMatch: 'full'
  // },
  // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    WelcomeComponent,
    PanelComponent,
    ChartViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    InlineSVGModule,
    MaterializeModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [
    AuthorizationService,
    AuthGuard,
    ChartService
  ],
  bootstrap: [AppComponent]
})



export class AppModule { }
