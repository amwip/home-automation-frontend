export const environment = {
  production: true
};

// Initialize Firebase
export const FIREBASE_CONFIG  = {
  apiKey: "AIzaSyBbojE6r8IAnoZYhWm4xBpk-6WUZ9cxsHk",
    authDomain: "smart-home-f6788.firebaseapp.com",
    databaseURL: "https://smart-home-f6788.firebaseio.com",
    projectId: "smart-home-f6788",
    storageBucket: "smart-home-f6788.appspot.com",
    messagingSenderId: "447106988636"
};