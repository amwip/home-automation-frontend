// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false
};

// Initialize Firebase
export const FIREBASE_CONFIG  = {
  apiKey: "AIzaSyBbojE6r8IAnoZYhWm4xBpk-6WUZ9cxsHk",
    authDomain: "smart-home-f6788.firebaseapp.com",
    databaseURL: "https://smart-home-f6788.firebaseio.com",
    projectId: "smart-home-f6788",
    storageBucket: "smart-home-f6788.appspot.com",
    messagingSenderId: "447106988636"
};