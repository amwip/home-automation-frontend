import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { IUserData } from '../interfaces/userData';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthorizationService {
  user: IUserData;

  constructor( public angularFireAuth: AngularFireAuth, public router: Router ) {}

  public isAuthenticated(): boolean {
    // check if refreshToken exists
    const token = localStorage.getItem('refreshToken');
    return !!token;
  }

  loginGoogleAccount() {
    this.angularFireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then( (result) => {
      console.log(result);
    });
  }

  loginFacebookAccount() {

  }

  login(email: string, password: string) {
    return Observable.fromPromise(this.angularFireAuth.auth.signInWithEmailAndPassword( email, password )
    .then(( response ) => {
      localStorage.setItem('refreshToken', response.refreshToken);
      return response;
    })
    .catch((error) => {
      console.log(error);
      throw error;
    }));
  }

  logout() {
    localStorage.removeItem('refreshToken');
    this.angularFireAuth.auth.signOut();
    this.router.navigate(['/']);
  }

  registerUser(email: string, password: string) {
    return Observable.fromPromise( firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(( response ) => {
      return response;
    })
    .catch((error) => {
      throw error;
    }));
  }
}
