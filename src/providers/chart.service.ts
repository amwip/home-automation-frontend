import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class ChartService {
  data: any;
  list: Subscription;

  constructor(private db: AngularFireDatabase) { }

  getData(dataset: string) {
    return this.db.list<any>(dataset).valueChanges();
  }

  // Used for realtime data updates
  // updateRanking(key, newData) {
  //   return this.db.object(`dht11/value/${key}`).update(newData)
  // }

  addRanking(newData) {
    // return this.db.object(`dht11/value/${key}`).update(newData)
  const dataList = this.db.list('`dht11/value/');
  let newPostRef = dataList.push(newData);
  this.db.object(`dht11/value/${newPostRef.key}`).update(newData)
  const listObservable = dataList.snapshotChanges();
  listObservable.subscribe();
  }

}
