import { Component, OnInit, EventEmitter } from '@angular/core';
import { AuthorizationService } from '../../providers/authorization.service';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menuItems: any[];

  constructor(public authorizationService: AuthorizationService) { }

  ngOnInit() {
    this.menuItems = [
      { route: 'panel', name: 'Panel'},
      { route: 'login', name: 'Logowanie'}
    ];
  }

  openSideMenu() {
    $('.button-collapse').sideNav('show');
  }

  logout() {
    this.authorizationService.logout();
    this.hideMenu();
  }

  hideMenu() {
    $('.button-collapse').sideNav('hide');
  }

}
