import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartService } from '../../providers/chart.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.component.html',
  styleUrls: ['./chart-view.component.css']
})
export class ChartViewComponent implements OnInit {
  DHTdata: any;
  DHTdataH: any;
  @ViewChild('chart1') el1: ElementRef;
  @ViewChild('chart2') el2: ElementRef;

  constructor(private chartService: ChartService) { }

  ngOnInit() {
    this.chartService.getData('dht11/value')
    .subscribe(items => {
      Plotly.purge(this.el1.nativeElement);
      Plotly.purge(this.el2.nativeElement);
      this.DHTdata = this.parseDataT(items);
      this.DHTdataH = this.parseDataH(items);
      this.topoChart1(this.DHTdata);
      this.topoChart2(this.DHTdataH);
    });
  }

  parseDataT(rawData) {
    let i = 0;
    let xData = [];
    let yData = [];
    let parsedData = [];

    rawData.forEach(element => {
      if (element.temperature === 'nan' || element.temperature == '0.00') {
        // do not show corrupted data
      } else {
        i++;
        element.temperature = parseFloat(element.temperature);
        element.timestamp = moment.unix(parseInt(element.timestamp)).format("YYYY-MM-DD HH:mm:ss");
        xData.push(element.timestamp);
        yData.push(element.temperature);
      }
    });

    parsedData[0] = {
      x: xData,
      y: yData
    };

    return parsedData;
}

parseDataH(rawData) {
  let i = 0;
  let xData = [];
  let yData = [];
  let parsedData = [];

  rawData.forEach(element => {
    if (element.humidity === 'nan' || element.humidity == '0.00') {
      // do not show corrupted data
    } else {
      i++;
      
      element.humidity = parseFloat(element.humidity);
      // element.timestamp = moment.unix(parseInt(element.timestamp)).format("YYYY-MM-DD HH:mm:ss");
      
      xData.push(element.timestamp);
      yData.push(element.humidity);
    }
  });

  parsedData[0] = {
    x: xData,
    y: yData
  };

  return parsedData;
}

  // basicChart() {
  //   const element = this.el1.nativeElement;

  //   const data = [{
  //     x: [1, 2, 3, 4, 5],
  //     y: [1, 2, 4, 8, 16]
  //   }]

  //   const style = {
  //     margin: { t: 0 }
  //   }

  //   Plotly.plot( element, data, style )
  // }

  topoChart1(data) {
    const element = this.el1.nativeElement
    const formattedData = [{
               z: data,
               type: 'scatter'
            }];

    const layout = {
      title: 'Temperatura DHT 11',
      autosize: true,
      width: 750,
      height: 350,
      margin: {
        l: 65,
        r: 50,
        b: 65,
        t: 90,
      }
    };

    Plotly.plot(element, data, layout);
  }

  topoChart2(data) {
    const element = this.el2.nativeElement
    const formattedData = [{
               z: data,
               type: 'scatter'
            }];

    const layout = {
      title: 'Wilgotność DHT 11',
      autosize: true,
      width: 750,
      height: 350,
      margin: {
        l: 65,
        r: 50,
        b: 65,
        t: 90,
      }
    };
    Plotly.plot(element, data, layout);
  }

  //random data to DB
  updateDataPoint() {
    const newData = {
      temperature: _.random(0, 40),
      humidity: _.random(0, 100),
      timestamp: Date.now()
    }
    this.chartService.addRanking(newData)
  }

}
