import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthorizationService } from '../../providers/authorization.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  modalData = {
    title: '',
    message: ''
  };
  user = {
    email: '',
    password: ''
  }

  constructor(public authorizationService: AuthorizationService, public router: Router) { }

  ngOnInit() {
    $(document).ready(function(){
      // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
      $('.modal').modal();
    });
  }

  login(f: NgForm) {
    this.authorizationService.login( f.value.email, f.value.password ).subscribe(
      data => {
        this.router.navigate(['panel']);
      },
      error => {
        this.modalData.title = 'Błąd';
        this.modalData.message = error.message;
        console.error(error);
        $('#modal1').modal('open');
      }
    );
  }

  register(f: NgForm) {
    this.authorizationService.registerUser( f.value.email, f.value.password ).subscribe(
      data => {
        this.modalData.title = 'Sukces';
        this.modalData.message = 'Użytkownik ' + data.email + ' został zarejestrowany.';
        $('#modal1').modal('open');
      },
      error => {
        this.modalData.title = 'Błąd';
        this.modalData.message = error.message;
        console.error(error);
        $('#modal1').modal('open');
      }
    );
  }

}
